package grammar;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Grammar {

	//	public Set<Rule> rules;
	public Production top;
	public Map<Production,List<List<Production>>> rules = new HashMap<Production, List<List<Production>>>();

	public static void main(String args[]){ 
		Grammar g = new Grammar();
	}
	
	public Grammar() {
		loadRules();
		this.top = new Production("start",false,false);
	}
	
	public Production getTop() {
		return top;
	}
	
	public void loadRules() {

		try {
			BufferedReader file = new BufferedReader(new FileReader("C:\\Users\\Luiza\\workspace\\projetoGit\\Earley\\repository\\java grammar.txt"));

			while (file.ready()) {
				String line = file.readLine();

				// ignores grammar comments and blank lines
				if (line.startsWith("/*") || line.matches("^\\s*$"))
					;

				// terminal rules are special
				else if (line.startsWith("#")) {

					String headAux = line.substring(line.indexOf("#") + 1,
							line.indexOf(":") - 1);
					Production head = new Production(headAux, false, false);
					String body = line.substring(
							line.indexOf(":") + 1, line.length());

					//					Rule newRule = new Rule(head, body, true, false);
					List<List<Production>> newRules = rules.get(head);
					if(newRules == null) {
						newRules = new ArrayList<List<Production>>();
					}

					for (String prod : body.split("\\|")) {
						List<Production> lstProd = new ArrayList<Production>();
						Production p = new Production(prod, true, false);
						lstProd.add(p);
						newRules.add(lstProd);
					}
					rules.put(head, newRules);


					// regex rules are special
				} else if (line.startsWith("%")) {

					String headAux = line.substring(line.indexOf("%") + 1,
							line.indexOf(":") - 1).trim();
					Production head = new Production(headAux, false, false);
					List<List<Production>> lstProd = rules.get(head);
					if(lstProd == null)
						lstProd = new ArrayList<List<Production>>();
					String roughRuleString = line.substring(
							line.indexOf(":") + 2, line.length()).trim();
					//					Rule newRule = new Rule(head, roughRuleString, true, true);
					List<Production> aux = new ArrayList<Production>();
					aux.add(new Production(roughRuleString,true,true));
					lstProd.add(aux);
					//					rules.add(newRule);
					rules.put(head, lstProd);

					// normal rules fit here
				} else {

					String headAux = line.substring(0,line.indexOf(":") - 1).trim();
					Production head = new Production(headAux, false, false);
					String roughRuleString = line.substring(
							line.indexOf(":") + 1,line.length()).trim();
					//					Rule newRule = new Rule(head, roughRuleString,false,false);
					List<List<Production>> prodAlt = rules.get(head);
					if(prodAlt==null)
						prodAlt = new ArrayList<List<Production>>();
					for (String body : roughRuleString.split("\\|")) {
						body = body.trim();
						List<Production> lstProd = new ArrayList<Production>();
						for (String prod : body.split(" ")) {
							Production p = new Production(prod.trim(), false, false);
							lstProd.add(p);
						}
						prodAlt.add(lstProd);
					}
					rules.put(head, prodAlt);
					//					rules.add(newRule);
				}
			}
			file.close();
//			System.out.println(toString());
		} catch (IOException e) {
			System.err.println("ERROR: GRAMMAR FILE IS INVALID! Exiting...");
			System.exit(0);
		}
	}

	@Override
	public String toString() {
		String ret = "";
		for (Production h : rules.keySet()) {
			List<List<Production>> prods = rules.get(h);
			for (List<Production> body : prods) {
				ret += (h + "\t->\t" + body.toString().replace("[", "").replace("]", "").replace(",", " +")) + "\n";
			}
		}
		return ret;
	}

	public boolean containsKey(Object key) {
		return rules.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return rules.containsValue(value);
	}

	public List<List<Production>> get(Production key) {
		return rules.get(key);
	}

	public boolean isEmpty() {
		return rules.isEmpty();
	}

	public Set<Production> keySet() {
		return rules.keySet();
	}

	public int size() {
		return rules.size();
	}

	public Collection<List<List<Production>>> values() {
		return rules.values();
	}

}
