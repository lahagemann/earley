package grammar;

import java.util.HashMap;
import java.util.Map;

public class Production {
	public String identifier;
	public boolean isterminal;
	public boolean isRegex;

	public Production(String identifier, boolean isterminal, boolean isRegex) {
		this.identifier = identifier.trim();
		this.isterminal = isterminal;
		this.isRegex = isRegex;
	}

	@Override
	public boolean equals(Object arg0) {
		if(arg0 instanceof Production)
			return ((Production)arg0).getIdentifier().equals(identifier);
		return false;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	@Override
	public String toString() {
		if(isterminal)
			return identifier.toUpperCase();
		return identifier.toLowerCase();
	}

	@Override
	public int hashCode() {
		return identifier.hashCode();
	}
	
	public static void main(String[] args) {
		Production p1 = new Production("p1", false, false);
		Production p2 = new Production("p1", false, false);
		Map<Production, Integer> map = new HashMap<Production, Integer>();
		map.put(p1, 10);
		System.out.println(map.keySet());
		System.out.println("p1".hashCode());
		System.out.println("p1".hashCode());
		System.out.println(map.get(p1));
		System.out.println(map.get(p2));
		
	}
}
