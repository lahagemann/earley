package grammar;
import java.util.ArrayList;
import java.util.List;

public class Rule {
	public String head;
	public List<Production> productions;

	public Rule(String rule, String roughProductionListAsAString,
			boolean isTerminal, boolean isRegex) {
		this.head = rule;
		this.productions = extractProductionsFromRule(
				roughProductionListAsAString, isTerminal, isRegex);
	}

	private List<Production> extractProductionsFromRule(String rules,
			boolean isTerminal, boolean isRegex) {
		List<Production> productions = new ArrayList<Production>();

		String[] separateRules = rules.split(" | ");
		for (String singleRule : separateRules) {
			Production newProduction = new Production(singleRule, isTerminal,
					isRegex);
			productions.add(newProduction);
		}
		return productions;
	}

}
