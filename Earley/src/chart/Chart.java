package chart;

import grammar.Grammar;
import grammar.Production;

import java.util.ArrayList;
import java.util.List;

public class Chart {

	List<State> states;
	String id;

	public Chart(String id) {
		this.id = id;
		states = new ArrayList<State>();
	}

	//	public Chart(Chart c, Grammar g, String id) {
	//		this.id = id;
	//		states = new ArrayList<State>();
	//		List<State> completed = c.getCompletedStates();
	//		for (State state : completed) {
	//			Production head = state.getHead();
	//			List<List<Production>> alternatives = g.get(head);
	//			for (List<Production> list : alternatives) {
	//				State newState = new State(head,list);
	//				states.add(newState);
	//				
	//			}
	//		}
	//	}
	//
	//	private List<State> getCompletedStates() {
	//		List<State> ret = new ArrayList<State>();
	//		for (State state : states) {
	//			if(state.isComplete()) 
	//				ret.add(state);
	//				
	//		}
	//		return ret;
	//	}

	public Chart(List<Production> heads, Chart c, Grammar g, String id) {
		this.id = id;
		states = new ArrayList<State>();
		for (State oldState : c.states) {
			if(!oldState.isComplete() && !oldState.isTerminal() && !heads.contains(oldState.getHead()))
				states.add(oldState);
		}
		List<State> aux = new ArrayList<State>();
		for (State state : states) {
			aux.addAll(state.propagate(g,id));
		}
		states.addAll(aux);

		//		

	}

	public void addState(State s) {
		states.add(s);
	}

	public void addState(List<State> s) {
		states.addAll(s);
	}

	@Override
	public String toString() {
		String ret = this.id.concat("\n");
		for (State state : states) {
			ret += "\t"+state+"\n";
		}
		return ret;
	}
	/* chart precisa ter uma lista/array de States que determinarão o progresso */

	public List<Production> consumeToken(String token) {
		List<Production> heads = new ArrayList<Production>();
		for (State state : states) {
			state.consumeToken(token);
			if(state.isComplete())
				heads.add(state.getHead());
		}
		return heads;

	}

	//	public List<State> moveDots(List<Production> heads) {
	//		List<Production> ret = new ArrayList<Production>();
	//		List<State> newStates = new ArrayList<State>();
	//		for (State state : states) {
	//			if(state.consumeToken(heads)) {
	//				newStates.add(state); 
	//			}
	//		}
	//		return newStates;
	//	}
	public List<Production> moveDots(List<Production> heads) {
//		List<Production> ret = new ArrayList<Production>();
//		ret.addAll(heads);
		List<State> newStates = new ArrayList<State>();
		boolean consumed = false;
		do{
			consumed = false;
			for (State state : states) {
				if(state.consumeToken(heads)) {
					newStates.add(state); 
					if(state.isComplete()){
						heads.add(state.getHead());
						consumed = true;
					}
				}
			}
		}while(consumed);
		return heads;
	}




}
