package chart;

import grammar.Grammar;
import grammar.Production;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Early {
	
	public static void main(String[] args) {
		Grammar grammar = new Grammar();
		Early early = new Early();
		Chart chart = early.start(grammar);
		System.out.println(chart);
		
		early.parse(Arrays.asList("public class String { }".split(" ")), grammar);
	}

	private ArrayList<State> startPoint;


	public boolean parse(List<String> program, Grammar g) {
		Chart c = start(g);
		
		for (int i = 0; i < program.size(); i++) {
			String token = program.get(i);
			System.out.println("leu token: " + token);
			List<Production> heads = c.consumeToken(token);
			List<Production> movedStates = c.moveDots(heads);
			System.out.println(c);
			if(heads.size()==0){
				System.err.println("n�o recohece");
				System.exit(1);
			}
				
			c = new Chart(movedStates,c,g,"c"+(i+1));
			System.out.println(c);
//			break;
		}
		for (State state : startPoint) {
			if(state.isComplete())
				return true;
		}
		return false;
	}

	private Chart start(Grammar g) {
		Production t = g.getTop();
		startPoint = new ArrayList<State>();
		Chart s0 = new Chart("c0");
		List<List<Production>> lst = g.get(t);		
		for (List<Production> list : lst) {
			State s = new State(t,list,"zero");
			startPoint.add(s);
			s0.addState(s);
			
			List<State> filhos = s.propagate(g,"zero");
			 s0.addState(filhos);
		}
		
		return s0;
	}
}
