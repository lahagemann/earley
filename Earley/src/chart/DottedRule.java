package chart;

import grammar.Rule;

public class DottedRule{

	int position;
	Rule correspondingRule;
	
	public DottedRule(Rule rule, int position) {
		this.position = position;
		this.correspondingRule = rule;
	}
	
	public DottedRule advanceDot(DottedRule dr) {
		return new DottedRule(dr.correspondingRule, dr.position++);
	}
	
	/*rule format looks like
	 * S -> @S + S
	 */
	@Override
	public String toString(){
		String rule = correspondingRule.head.concat(" ->");
		for (int i = 0; i < correspondingRule.productions.size(); i++) {
			if(i == position) {
				rule.concat(" @");
				rule.concat(correspondingRule.productions.get(i).identifier);
			} else {
				rule.concat(" ");
				rule.concat(correspondingRule.productions.get(i).identifier);
			}
		}
		return rule;
	}
	


}
