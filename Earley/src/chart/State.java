package chart;

import grammar.Grammar;
import grammar.Production;
import grammar.Rule;

import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class State {

	public int dot;
	public List<Production> body;
	public Production head; //retornada como token reconhecido quando o estado est� completo
	private boolean isTerminal;
	private String created;

	public State(Production current, List<Production> list, String created) {
		this.created = created;
		this.body = list;
		this.head = current;
		for (Production production : list) 
			if(production.isterminal){
				isTerminal = true;
				break;
			}

	}

	//	public void setBody(List<Production> list) {
	//		this.body = list;		
	//	}
	//
	//	public void setHead(Production t) {
	//		this.head = t;
	//	}

	public List<State> propagate(Grammar g, String created) {
		List<State> listOfNewStates = new ArrayList<State>();
		Production current = body.get(dot);
		if(current.isterminal) {
			//
		} else {
			List<List<Production>> alternates = g.get(current);
			for (List<Production> list : alternates) {
				State newState = new State(current,list, created);
				listOfNewStates.add(newState);
				listOfNewStates.addAll(newState.propagate(g,created));
			}
		}		
		return listOfNewStates;
	}

	@Override
	public String toString() {
		String ret = head+"/"+dot+"\t"+body + "\tfrom: " + created;
		return ret;
	}

	public void consumeToken(String token) {
		Production candidate = body.get(dot);
		if(candidate.isterminal && candidate.identifier.equals(token)) {
			dot++;

		}
	}

	public boolean isComplete() {
		return dot == body.size();
	}
	public Production getHead() {
		return head;
	}

	public boolean consumeToken(List<Production> heads) {
		if(isComplete())
			return false;
		Production current = body.get(dot);
		if(!current.isterminal)
			for (Production production : heads) {
				if(production.equals(current)) {
					dot++;
					return true;
				}
			}
		return false;
	}

	public boolean isTerminal() {
		return isTerminal;
	}



}
