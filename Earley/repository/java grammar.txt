/* java grammar (not bnf format): */


/* class initializer - main program construction */

start : class_initializer
class_initializer : modifier class identifier class_body
class_body : l_bracket optional_class_declarations r_bracket
optional_class_declarators : empty | class_declarations
class_declarations : class declaration | class_declaration class_declarations
class_declaration : method

/* method */

method : method_header block
method_header : modifier type identifier l_parenthesis r_parenthesis | modifier void identifier l_parenthesis r_parenthesis

/* blocks */

block : l_bracket r_bracket

/* types */

type : class_name | boolean | double | float | int | string



/* modifiers */
modifier : public | private | protected | new

/* name rules - % means terminal is regex and should be used as such */
identifier : var_name | class_name
% class_name : ^[A-Z]([a-z]+[A-Z])*
% var_name : ^[a-z]([a-z]+[A-Z]+[0-9])* 

/* terminals */
# boolean : boolean
# break : break
# class : class
# do : do
# double : double
# else : else
# false : false
# float : float
# for : for
# int : int | Integer
# new : new
# null : null
# private : private
# protected : protected
# public : public
# return : return
# string : String
# true : true
# void : void
# while : while

# comma : ,
# l_bracket : {
# r_bracket : }
# l_parenthesis : (
# r_parenthesis : )
# empty : 

# add_assign : +=
# div_assign : /=
# mod_assign : %=
# mul_assign : *=

# le_op : <= | =<
# ge_op : >= | =>
# lt_op : <
# gt_op : >
# eq_op : ==
# ne_op : !=

# and_op : &&
# or_op : OR
# inc_op : ++
# dec_op : --

